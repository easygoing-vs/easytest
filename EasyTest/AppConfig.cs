﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;
using System.Xml.Serialization;

namespace EasyTest
{
    public sealed class AppConfig
    {
        private static AppConfig instance = null;
        private static readonly object padlock = new object();
        public static AppConfig Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new AppConfig();
                    }
                    return instance;
                }
            }
        }

        Page1 page1;

        [XmlElement("Page1")]
        public string Page1Title
        {
            get
            {
                return page1.myText.Text;
            }

            set
            {
                page1.myText.Text = value;
            }
        }

        Page2 page2;

        [XmlElement("Page2")]
        public string Page2Title
        {
            get
            {
                return page2.myText.Text;
            }

            set
            {
                page2.myText.Text = value;
            }
        }

        private AppConfig()
        {
            this.page1 = new Page1();
            this.page2 = new Page2();
        }

        public void Save()
        {
            XmlSerializer ser = new XmlSerializer(typeof(AppConfig));

            try
            {
                TextWriter writer = new StreamWriter(@"C:\Users\vukasin\Documents\Source\Repos\EasyTest/AppConfig.xml");
                ser.Serialize(writer, instance);
                writer.Close();
            }
            catch (Exception)
            {
                throw new Exception("Not cool!");
            }
            
        }

        public AppConfig Reload()
        {
            string path = @"C:\Users\vukasin\Documents\Source\Repos\EasyTest/AppConfig.xml";

            XmlSerializer ser = new XmlSerializer(typeof(AppConfig));

            StreamReader reader = new StreamReader(path);
            instance = (AppConfig)ser.Deserialize(reader);
            reader.Close();

            return instance;
        }

    }
}
