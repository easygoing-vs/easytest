﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasyTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            AppConfig.Instance.Save();
            this.myFrame.NavigationUIVisibility = System.Windows.Navigation.NavigationUIVisibility.Hidden;
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            this.myFrame.Content = AppConfig.Instance.Reload().Page1Title;
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            this.myFrame.Content = AppConfig.Instance.Reload().Page2Title;
        }

        private void insertText_Click(object sender, RoutedEventArgs e)
        {
            InputBox.Visibility = System.Windows.Visibility.Visible;
            this.myFrame.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void InputButton_Click(object sender, RoutedEventArgs e)
        {
            // YesButton Clicked! Let's hide our InputBox and handle the input text.
            InputBox.Visibility = System.Windows.Visibility.Collapsed;
            this.myFrame.Visibility = System.Windows.Visibility.Visible;

            // Do something with the Input
            String input = InputTextBox.Text;
            AppConfig.Instance.Page1Title = input;
            AppConfig.Instance.Save();

            // Clear InputBox.
            InputTextBox.Text = String.Empty;
        }
    }
}
 